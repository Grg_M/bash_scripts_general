#!/bin/bash
# A sample shell script to change the extensions of files ina d irectory

#---------------------------------------------------------------------------------------
# ARGUMENTS FROM COMMAND LINE:
#---------------------------------------------------------------------------------------

# 1) extension of the files
INEXTEN=$1

# 2) path to the directory
INDIR=$2

# 3) path to new directory
OUTDIR=$3

# 4) new extension for the file
OUTEXTEN=$4

# 5) pattern to exclude
PATTERN=$5

#---------------------------------------------------------------------------------------
# EXECUTE
#---------------------------------------------------------------------------------------

# build the list of files
FILES=$(find "$INDIR" -maxdepth 1 -type f -not -name "*$PATTERN*" -name "*$INEXTEN")

for f in $FILES
do
	echo "converting $f file.."
	cp $f $OUTDIR/$(basename "$f" $INEXTEN)$OUTEXTEN
	
done
